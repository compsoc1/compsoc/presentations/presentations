+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Virtual Machines!"
date    = 2024-04-17
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Virtual Machines
-------------------------------------------------------------------------
### Haaaiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
-------------------------------------------------------------------------
### *cough*
-------------------------------------------------------------------------
### Hello.
-------------------------------------------------------------------------
What IS A VIRTUAL MACHINE????
-------------------------------------------------------------------------
So what *are* they?
-------------------------------------------------------------------------
![img.jpg](2_vms/yap.jpg)
-------------------------------------------------------------------------
Imagine the following....
-------------------------------------------------------------------------
![img.jpg](2_vms/matryoshka.jpg)
-------------------------------------------------------------------------
And yes, before you ask. It's possible to run nested VMs.
-------------------------------------------------------------------------
Its like making your computer pretend its gone a second, smaller computer inside it
-------------------------------------------------------------------------
### And for those of us that use the wrong OS, you can always run Windows VMs if you want to use anything with >3 users
-------------------------------------------------------------------------
### Imagine the following:
-------------------------------------------------------------------------
![img.jpg](2_vms/vault.jpg)
-------------------------------------------------------------------------
[FIND SOME WAY TO PUT THE VIDEO IN HERE]
-------------------------------------------------------------------------
### So, how do you stop small asian gymnasts from rooting around in your PC where it shouldn't be?
-------------------------------------------------------------------------
### Virtual Machines! It's like nailing this box shut!
-------------------------------------------------------------------------
### (Ignore the implications of that)
-------------------------------------------------------------------------
So when you do this....
-------------------------------------------------------------------------
![img.jpg](2_vms/downloadmoreram.jpg)
-------------------------------------------------------------------------
You can just kill that sandbox, and spin up a new one!
-------------------------------------------------------------------------
### For today, we'll be using [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
-------------------------------------------------------------------------
### Ignore the somewhat-ancient looking layout
-------------------------------------------------------------------------
### Download the installer for your OS, get it, run it, you know the drill.
-------------------------------------------------------------------------
### Run the program, you'll get this screen
-------------------------------------------------------------------------
![img.jpg](2_vms/VBoxMainScreen.jpg)

-------------------------------------------------------------------------
### For today, we'll try out some new OSes. Go [here](https://templeos.org/Downloads/) for TempleOS, or [here](https://uwuntuos.site/downloads/) for UwUntu. We'll start out with TempleOS, because its lighter and quicker to download.
-------------------------------------------------------------------------
### Download the .iso files, and put them in a folder that you can easily access. 
-------------------------------------------------------------------------
### Next, go back to the VirtualBox application
-------------------------------------------------------------------------
![img.jpg](2_vms/Machine.jpg)
-------------------------------------------------------------------------
![img.jpg](2_vms/CreateVM.jpg)
-------------------------------------------------------------------------
![img.jpg](2_vms/VMHardware.jpg)
-------------------------------------------------------------------------
### Try not to allocate your entire memory. That's *generally* considered a bad idea. 
-------------------------------------------------------------------------
![img.jpg](2_vms/VirtualHardDisk.jpg)
-------------------------------------------------------------------------
### Hit 'Finish'
-------------------------------------------------------------------------
### And now, you've got the following!:
-------------------------------------------------------------------------
![img.jpg](2_vms/VMPoweredOff.jpg)
-------------------------------------------------------------------------
### Hit "Start", let your machine power up....
-------------------------------------------------------------------------
### And from here, explore the OS! And if your PC is on fire, you've done something wrong. *Most of the time*, at least.
-------------------------------------------------------------------------