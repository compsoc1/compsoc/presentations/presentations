+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Minecraft Setup"
date    = 2024-02-27
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

<h2 style="text-align: center;">How to setup the in-game voice chat</h2>
<p style="text-align: center;">so you can talk to other members in the CompSoc Minecraft Server</p>

-------------------------------------------------------------

<p style="text-align: center;">You can open this presentation on:</p>
<p style="text-align: center;"> <a href="https://public.skynet.ie">public.skynet.ie</a> > slides > compsoc > 0_mc-setup.html</p>
<p style="text-align: center;">to access the links you need.</p>

----------------------------------------------------------------

<h2 style="text-align: center;">Setup in the official Minecraft Launcher</h2>
<p style="text-align: center;">I'll also show you how to do it in **MultiMC** later.</p>

----------------------------------------------

<p style="text-align: center;">Go to <a href="https://fabricmc.net">fabricmc.net</a> and click the <b>Download</b> button:</p>
<img src="0_mc-setup/01_fabric-download-1.png"></img>

-----------------------------------------------------------

<p style="text-align: center;">Then click the <b>Download</b> button again:</p>
<img src="0_mc-setup/02_fabric-download-2.png"></img>

---------------------------------------------

<p style="text-align: center;">Run the downloaded installer and click <b>Install</b>:</p>
<img src="0_mc-setup/03_install-fabric.png"></img>

-------------------------------------------------------------------------

<p style="text-align: center;">Next, go to <a href="https://modrinth.com/plugin/simple-voice-chat/versions?g=1.20.4&l=fabric">this</a> link or to <a href="https://modrinth.com">modrinth.com</a> then mods,</p>
<p style="text-align: center;">search for Simple Voice Chat, then the Versions tab.</p>

<p style="text-align: center;">Now download the latest version for 1.20.4 fabric.</p>

<p style="text-align: center;">Also do this if you're using MultiMC.</p>

-------------------------------------------------------------------------

<p style="text-align: center;">Open the Minecraft Launcher and go to the <b>Installations</b> tab:</p>
<img src="0_mc-setup/04_open-launcher.png"></img>

-----------------------------------

<p style="text-align: center;">Now open the new version's folder:</p>
<img src="0_mc-setup/05_open-folder.png"></img>

----------------------------------

<p style="text-align: center;">Move the downloaded mod into the folder called <b>mods</b>:</p>
<img src="0_mc-setup/06_mods-folder.png"></img>

-------------------------------------------------------------------------

<p style="text-align: center;">And you're done!</p>
<p style="text-align: center;">You can launch the Fabric Minecraft version</p>
<p style="text-align: center;">and join the server using the instructions on <a href="https://games.skynet.ie">games.skynet.ie</a>.</p>

-------------------------------------------------------------------------

<h2 style="text-align: center;">MultiMC setup</h2>

--------------------------------------------------

<p style="text-align: center;">Run MultiMC and create a new instance:</p>
<img src="0_mc-setup/07_multimc-instance.png"></img>

------------------------------------------------

<p style="text-align: center;">Select Vanilla 1.20.4 and click <b>Ok</b>:</p>
<img src="0_mc-setup/08_select-version.png"></img>

-----------------------------------------

<p style="text-align: center;">Now select the version and click on <b>Edit Instance</b></p>
<p style="text-align: center;">in the menu on the right side:</p>
<img src="0_mc-setup/09_edit-instance.png" height="120"></img>

----------------------------------------

<p style="text-align: center;">Click on <b>Install Fabric</b> then install the latest version with a star:</p>
<img src="0_mc-setup/10_multimc-fabric.png"></img>

-----------------------------------------

<p style="text-align: center;">Go to the <b>Loader mods</b> menu:</p>
<img src="0_mc-setup/11_copy-mod-multimc.png" height="60"></img>
<p style="text-align: center;">And add the downloaded mod file:</p>
<img src="0_mc-setup/12_add-mod.png" height="60"></img>

-------------------------------------------------------------------------

<p style="text-align: center;">And you're done!</p>
<p style="text-align: center;">You can launch the Fabric Minecraft version</p>
<p style="text-align: center;">and join the server using the instructions on <a href="https://games.skynet.ie">games.skynet.ie</a>.</p>
