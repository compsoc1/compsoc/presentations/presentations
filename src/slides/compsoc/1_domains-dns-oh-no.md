+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Domains, DNS, oh no"
date    = 2024-04-03
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Domains, DNS, oh no

-------------------------------------------------------------------------

### Hello Class of CS4116

-------------------------------------------------------------------------

So we use domains every day, even if ye dont know it

-------------------------------------------------------------------------

52.30.181.100

-------------------------------------------------------------------------

52.30.181.100 - Brightspace

-------------------------------------------------------------------------

209.85.203.94

-------------------------------------------------------------------------

209.85.203.94 - Google

-------------------------------------------------------------------------

Those numbers arent very human friendly

-------------------------------------------------------------------------

So what is teh solution?

-------------------------------------------------------------------------

## Getting a domain!

-------------------------------------------------------------------------

There are many domain hosts to go for

-------------------------------------------------------------------------

And prices range from &euro;1/year to &euro;infinity/year

-------------------------------------------------------------------------

* .ie
* .com
* .tk
* .coffee
* .eu
* .me
* .gay
* ...

-------------------------------------------------------------------------

There is even .cat, though that is off limits to most of us.....

-------------------------------------------------------------------------

I recommend using <<https://www.blacknight.com/>> for .ie

-------------------------------------------------------------------------

And <<https://www.namecheap.com/>> for everything else

-------------------------------------------------------------------------

I am going to focus on .ie

-------------------------------------------------------------------------

These will set ye back ~&euro;7 for the first year.

-------------------------------------------------------------------------

Then &euro;30 for every year afterwards

-------------------------------------------------------------------------

Enter your perfect domain name into the big box

-------------------------------------------------------------------------

![img.png](1_domains/img01.png)

-------------------------------------------------------------------------

It will then show ye a list of options

-------------------------------------------------------------------------

![img.png](1_domains/img02.png)

-------------------------------------------------------------------------

Select one, add it to the cart

-------------------------------------------------------------------------

In the config select ``I'm hosting my website somewhere else``

-------------------------------------------------------------------------

Then add these two as nameservers: 
* ``nadia.ns.cloudflare.com``
* ``neil.ns.cloudflare.com``


Press ``Update`` then ``Checkout`` for payment

-------------------------------------------------------------------------

You will verify that ye have a connection to Ireland

-------------------------------------------------------------------------

Your Student ID will work for this

-------------------------------------------------------------------------

Couldnt find a name that suited you in ASCII text?

-------------------------------------------------------------------------

Have no fear!

-------------------------------------------------------------------------

## Punycode to the rescue!

-------------------------------------------------------------------------

Say you want ``brend&aacute;n.ie``

-------------------------------------------------------------------------

But because of the ``&aacute;`` it is not a "normal" domain.

-------------------------------------------------------------------------

Go to <<https://www.punycoder.com/>> and enter in your special domain

-------------------------------------------------------------------------

Boom: ``xn--brendn-tta.ie``

-------------------------------------------------------------------------

Or say ye wanted to go wild and have an Ogham domain name?

-------------------------------------------------------------------------

``&#x1681;&#x168f;&#x1693;&#x1685;&#x1687;&#x1690;&#x1685;.com`` for example (brendan in Ogham)

[0]: https://gitlab.skynet.ie/compsoc1/compsoc/presentations/presentations/-/blob/ceb346fe8e8dfc553fff520de7864e96236e887a/src/slides/compsoc/1_domains-dns-oh-no.md#L163

-------------------------------------------------------------------------

``&#x1681;&#x168f;&#x1693;&#x1685;&#x1687;&#x1690;&#x1685;.com`` becomes ``xn--7ueiah2bis.com``

-------------------------------------------------------------------------

Magic!

-------------------------------------------------------------------------

And congrats! You now have a domain.

-------------------------------------------------------------------------

As we saw in the earlier slides a domain points to an IP address.

-------------------------------------------------------------------------

Ye can point your domain to another domain.

-------------------------------------------------------------------------

These go to the same location
* <<https://brendan.ie>>
* <<https://brendan.coffee>>

-------------------------------------------------------------------------

So anyone in CS4116 would love to point  
<<https://hi-conor-ryan-please-give-us-an-a1.ie>>   
To their InfinityFree instance

-------------------------------------------------------------------------

## For that we need DNS

InfinityFree has their own [(good) docs][1] for this.  
But I prefer to use [CNAME for this][2]

[1]: https://forum.infinityfree.com/docs?topic=49232
[2]: https://forum.infinityfree.com/docs?topic=87420

-------------------------------------------------------------------------

Cloudflare is one of teh best DNS providers

-------------------------------------------------------------------------

<<https://cloudflare.com>>

-------------------------------------------------------------------------

Signup, you should know how to do this.....

-------------------------------------------------------------------------

On teh homepage select ``Add a site`` 
![img.png](1_domains/img03.png)

-------------------------------------------------------------------------

Enter in your domain (``hi-conor-ryan-give-me-a1.ie``)

-------------------------------------------------------------------------

Will take little while for it to be activated.  

-------------------------------------------------------------------------

So go make a cuppa.

-------------------------------------------------------------------------

If ye are using InfinityFree checkout their [CNAME docs][2]

-------------------------------------------------------------------------

Thats kinda the end of this, it is targeted to CS4116.