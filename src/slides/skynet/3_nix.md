+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Skynet: Nix"
date    = 2023-10-08
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# *nix, Nix and Nixos

-------------------------------------------------------------------------

## *nix

-------------------------------------------------------------------------

Skynet runs on a flavor of Linux called NixOS

-------------------------------------------------------------------------

Normal linux commands apply (basic primer)

-------------------------------------------------------------------------

``cd`` - change directory

-------------------------------------------------------------------------

``mkdir`` - make directory

-------------------------------------------------------------------------

``ls`` - list directory

-------------------------------------------------------------------------

``touch {filename}`` - create file named {filename}

-------------------------------------------------------------------------

``nano {filename}`` - edit {filename}

-------------------------------------------------------------------------

``history`` - view history of previous commands

-------------------------------------------------------------------------

``grep "{query}" {filename/path}`` - find {query} in a file

-------------------------------------------------------------------------

There is also piping where:  
The output of one command is piped into another command

-------------------------------------------------------------------------

Often called [Unix philosophy][1]

-------------------------------------------------------------------------

Can make really powerful programs from smaller simple programs.

-------------------------------------------------------------------------

``history | grep "nano"`` - search the history for any mention of ``nano``

-------------------------------------------------------------------------

## Nix

-------------------------------------------------------------------------

Nix is a (lazy) functional language

-------------------------------------------------------------------------

```nix
a = 1              # int
b = 1.001          # float
c = /path/to/thing # path
d = "42"           # string
e = true           # boolean
```

-------------------------------------------------------------------------

```nix
double = x: x*2
mul = a: b: a*b

double 2
mul 2 3
```

-------------------------------------------------------------------------

```nix
s = { foo = "bar"; biz = "baz"; }
s.foo # bar
s.biz # baz
```

-------------------------------------------------------------------------

Info:
1. [Offical guide][2]
2. [Nix Pills][3]

-------------------------------------------------------------------------

## NixOS

-------------------------------------------------------------------------

Some crazy person saw Nix and thought "I want to make an OS with that"

-------------------------------------------------------------------------

In essence a giant function is created with an OS as the output

-------------------------------------------------------------------------

This does have quite a few advantages

-------------------------------------------------------------------------

* Config as Code
* Deterministic
* Reproducible

-------------------------------------------------------------------------

Skynet 2.0 had its config spread across different servers.  
Making it hard to get a good overview

-------------------------------------------------------------------------

Skynet 3.0 is fully source controlled on [gitlab.skynet.ie][4]

-------------------------------------------------------------------------

Deterministic and Reproducible go hand in hand.

-------------------------------------------------------------------------

Deterministic means that for the same inputs you get the same output.

-------------------------------------------------------------------------

Reproducible is that you are able to create the same output from  
the source code.

-------------------------------------------------------------------------

We use Flakes, which adds a lockfile, reduces hassle for the dev.

-------------------------------------------------------------------------

Questions?

-------------------------------------------------------------------------


[1]: https://en.wikipedia.org/wiki/Unix_philosophy
[2]: https://nix.dev/tutorials/first-steps/
[3]: https://nixos.org/guides/nix-pills/#
[4]: https://gitlab.skynet.ie/compsoc1/skynet/nixos