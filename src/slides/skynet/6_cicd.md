+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Skynet: CI/CD"
date    = 2023-10-28
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# CI/CD

-------------------------------------------------------------------------

CI: Continuous Integration  
CD: Continuous Delivery/Deployment

-------------------------------------------------------------------------

## CI: Continuous Integration

-------------------------------------------------------------------------

This is regularly testing and merging into the main branch.

-------------------------------------------------------------------------

Anyone contributing to skynet will have seen the test/linter pipelines.

-------------------------------------------------------------------------

Its easier/less painful to test small changes.

-------------------------------------------------------------------------

## CD: Continuous Delivery/Deployment

-------------------------------------------------------------------------

Automatically building or deploying teh final product after CI

-------------------------------------------------------------------------

Builds/deploys in a reliable manner.

-------------------------------------------------------------------------

## Why its so danm good

-------------------------------------------------------------------------

So you have been using Git right?

-------------------------------------------------------------------------

But you have been SSHing into teh server to update the code in production

-------------------------------------------------------------------------

Which is grand but tedious.

-------------------------------------------------------------------------

You forgot to push the new update last ween to the server.

-------------------------------------------------------------------------

Now its Friday and ye got DnD this evening.

-------------------------------------------------------------------------

So ye quicky pull ghe git repo in and run the init script.

-------------------------------------------------------------------------

Then ye quickly leave, thinking of what your Fairy Barbarian will do next

-------------------------------------------------------------------------

You may have forgotten to test it properly though.....

-------------------------------------------------------------------------

```bash
userDir=/home/username
rm -rf $userdir/
```

-------------------------------------------------------------------------

Congratulations!

-------------------------------------------------------------------------

You just wiped everything!

-------------------------------------------------------------------------

Did you spot the typo?

-------------------------------------------------------------------------

So what are the things that went wrong?

-------------------------------------------------------------------------

1. Forgot to deploy the previous release.
2. Forgot to do/skipped testing.
3. It will be Sat that you will get teh dreaded phonecall.

-------------------------------------------------------------------------

CI/CD in this case will test and then deploy, automagically.

-------------------------------------------------------------------------

If it finds an error the pipeline stops.

-------------------------------------------------------------------------

Some pipelines have the option to undo a bad deploy.

-------------------------------------------------------------------------

## CI/CD as a force multiplier.

-------------------------------------------------------------------------


It allows Devs to deploy early and often (feel like making an impact)

-------------------------------------------------------------------------

Gives Ops folks more reliability, same process every time.


-------------------------------------------------------------------------

Handles tediousness.

-------------------------------------------------------------------------

It does require more up front time investment to get set up.

-------------------------------------------------------------------------

Well worth it for piece of mind.

-------------------------------------------------------------------------

Back in 2009 there were [4+ admins][0]

-------------------------------------------------------------------------

They were each responsible for their own servers.

-------------------------------------------------------------------------

Each had their own processes.

-------------------------------------------------------------------------

Handed down like arcane scrolls.

-------------------------------------------------------------------------

Now said scrolls are codified.

-------------------------------------------------------------------------

Since pipeline deploys on our behalf no need to have admin access.

-------------------------------------------------------------------------


//Use this time to explore the nixos repo to explain//



[0]: https://2016.skynet.ie/contacts.html