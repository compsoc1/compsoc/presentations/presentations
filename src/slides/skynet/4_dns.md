+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Skynet: DNS"
date    = 2023-10-14
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


# DNS

-------------------------------------------------------------------------

## Overview

-------------------------------------------------------------------------

DNS is both simple and complex.

-------------------------------------------------------------------------

DNS translates the human domains into something computers can understand.

-------------------------------------------------------------------------

```dns
skynet  IN A     193.1.96.165
*.users IN CNAME skynet
```

-------------------------------------------------------------------------

It can also provide useful information.

-------------------------------------------------------------------------

```dns
skynet.ie. IN TXT   "v=spf1 a:mail.skynet.ie -all"
```

-------------------------------------------------------------------------

## Structure

-------------------------------------------------------------------------

DNS is hierarchical and delegated.

-------------------------------------------------------------------------

Root servers

-------------------------------------------------------------------------

TLD (Top Level Domain) servers (``.ie``, ``.com``, ``.eu``)

-------------------------------------------------------------------------

Domain servers (``skynet.ie``)

-------------------------------------------------------------------------

We run our own DNS servers, Bind9 (``ns1.skynet.ie``, ``ns2.skynet.ie``)

-------------------------------------------------------------------------

We also have control over ``csn.ul.ie`` and ``ulcompsoc.ie``

-------------------------------------------------------------------------

## Viewing DNS records
 
-------------------------------------------------------------------------

On the nixos repo:
```bash
colmena build --keep-result --on vendetta
cat .gcroots/node-vendetta/etc/skynet/dns/skynet.ie
```

-------------------------------------------------------------------------

On top of that we also manage the reverse DNS (rDNS)

-------------------------------------------------------------------------

Reverse DNS allows you to look up and IP and get the domains.

-------------------------------------------------------------------------

```bash
cat .gcroots/node-vendetta/etc/skynet/dns/64-64.99.1.193.in-addr.arpa
```

-------------------------------------------------------------------------

To validate them there is this tool: <<https://bind.jamiewood.io/>>  
(put ``skynet.ie`` in teh FQDN box)

-------------------------------------------------------------------------

## Commands

-------------------------------------------------------------------------

```bash
dig skynet.ie
dig skynet.ie @1.1.1.1
dig skynet.ie @ns1.skynet.ie
host 193.1.99.75
```

-------------------------------------------------------------------------

//Use this time to explore the nixos repo to explain//

-------------------------------------------------------------------------

* <<https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/issues/21>>
* <<https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/issues/33>>

