+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Skynet: Proxmox"
date    = 2023-11-19
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Proxmox

-------------------------------------------------------------------------

Proxmox is a primarily a virtualization platform.

-------------------------------------------------------------------------

Open source.

-------------------------------------------------------------------------

Built on Debian. (all normal linux commands work)

-------------------------------------------------------------------------

Suitable for both homelab and datacenter.

-------------------------------------------------------------------------

Allows clustering of physical machines.

-------------------------------------------------------------------------

Uses a webgui to manage/monitor.

-------------------------------------------------------------------------

Also can manage ZFS storage.

-------------------------------------------------------------------------

Can manage VM's and LXC's

-------------------------------------------------------------------------

We mostly use LXC's (got a base template set up)

-------------------------------------------------------------------------

There is a root login, but recommended to use yer skynet account.

-------------------------------------------------------------------------

# Demo Time

-------------------------------------------------------------------------

## Creating a new LXC (Proxmox)

1. Login to Proxmox
2. Top right, Create Container
3. Set Hostname (example), Upload Admin public key
4. Select template
5. Set Disk/CPU/Memory
6. Set IPv4 address and gateway (193.1.99.xyz/26, 193.1.99.65)
7. Finish

-------------------------------------------------------------------------

## Creating a new LXC (Nixos config)

1. Create a new Machine
2. Set hostname and IP
3. Add to ``flake.nix``
4. Bootup the server in Proxmox and ssh into it (using above key)
5. ``cat /etc/ssh/ssh_host_ed25519_key.pub`` into ``/secrets/secrets.nix``
6. ``agenix -r`` - grant the new server access to secrets.
7. ``colmena apply --on @dns`` - Update records
8. ``colmena apply --on example``

-------------------------------------------------------------------------

//Use this time to explore the nixos repo to explain//
