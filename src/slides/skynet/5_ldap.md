+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Skynet: LDAP"
date    = 2023-10-22
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# LDAP

-------------------------------------------------------------------------

Lightweight Directory Access Protocol

-------------------------------------------------------------------------

Basically the digital version of a phonebook

-------------------------------------------------------------------------

A type of database

-------------------------------------------------------------------------

Stores the basic account information.

-------------------------------------------------------------------------

Technically //the// Skynet account.

-------------------------------------------------------------------------

<img src="5_ldap_database.png" style="max-width: 450px; max-height: 200px" src="image.png"></img>

-------------------------------------------------------------------------

Stores data that is useful for differences services

-------------------------------------------------------------------------

A source of truth.

-------------------------------------------------------------------------

While it is possible to use terminal commands

-------------------------------------------------------------------------

Using a dedicated tool is far more intuitive/better.

-------------------------------------------------------------------------

I recommend this, works on windows/linux.  
https://directory.apache.org/studio/

-------------------------------------------------------------------------

//Use this time to explore the nixos repo to explain//

