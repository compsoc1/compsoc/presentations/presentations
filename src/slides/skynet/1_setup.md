+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Skynet: Setup"
date    = 2023-09-23
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Skynet: Setup

-------------------------------------------------------------------------

Now that ye have the history time to get you Skynet accounts.

-------------------------------------------------------------------------

This is for new members.  
If there are any older members please wait a little while.

-------------------------------------------------------------------------

## New Members

-------------------------------------------------------------------------

1. Be a fully paid up member of UL Computer Society

-------------------------------------------------------------------------

2. Go to <<https://account.skynet.ie/signup>>.  
   Enter the same email used for UL Wolves

-------------------------------------------------------------------------

3. You will get an email to verify your address, follow the link.

-------------------------------------------------------------------------

4. Choose a ``username`` and ``password``.

-------------------------------------------------------------------------

Congrats! You are in! //heist music//

-------------------------------------------------------------------------

## Returning Members

-------------------------------------------------------------------------

1. Please go to <<https://account.skynet.ie/modify>>.  
   Enter the same email used for UL Wolves

-------------------------------------------------------------------------

Congrats! You are in! //heist music//

-------------------------------------------------------------------------

## Returning Members

-------------------------------------------------------------------------

If you have forgotten your ``username``.  
Use <<https://account.skynet.ie/recover/username>>

-------------------------------------------------------------------------

If you have forgotten your ``password``.  
Use <<https://account.skynet.ie/recover/password>>

-------------------------------------------------------------------------

## All Members

-------------------------------------------------------------------------

Now that we are all on the same page we can now set an SSH key.

-------------------------------------------------------------------------

Prepwork: 
```bash
cd ~/.ssh
mkdir skynet
cd skynet
```

-------------------------------------------------------------------------

```bash
ssh-keygen -t ed25519 -C "<comment>"
```

Location: username  
Password: Press Enter twice

We will use the ``{username}.pub``  file later

-------------------------------------------------------------------------

Back to the ssh folder
```bash
cd ../
```
-------------------------------------------------------------------------

Windows
```powershell
"" > config
```


Linux
```bash
touch config
```

-------------------------------------------------------------------------

Into it pop this 
```
Host *.skynet.ie
   User %r
   IdentityFile ~/.ssh/skynet/%r
   IdentitiesOnly yes
```
(we will also ping this on discord)

-------------------------------------------------------------------------

Remember the ``{username}.pub`` in ``~/.ssh/skynet`` ?

-------------------------------------------------------------------------

```bash
cat ~/.ssh/skynet/{username}.pub
```

-------------------------------------------------------------------------

Copy the text and go to <<https://account.skynet.ie/modify>>

-------------------------------------------------------------------------

Select the SSH Key field and submit.

-------------------------------------------------------------------------

Congratulations, you now have full access to all of Skynet's services!

-------------------------------------------------------------------------

Now to learn //how// to access them!

-------------------------------------------------------------------------

## Email

-------------------------------------------------------------------------

1. Go to <<https://mail.skynet.ie/>>
2. Login with ``username@skynet.ie`` and ``password``
3. Done

-------------------------------------------------------------------------

## Email (Thunderbird)

-------------------------------------------------------------------------

0. Have Thunderbird Installed
1. Add Account
2. Login with ``username@skynet.ie`` and ``password``
3. It can take a few min.
4. Done

-------------------------------------------------------------------------

## Skynet Webserver

-------------------------------------------------------------------------

```bash
ssh {username}@skynet.skynet.ie
```

-------------------------------------------------------------------------

Thats it, you have logged into Skynet (webserver)!

-------------------------------------------------------------------------

## Skynet Website

-------------------------------------------------------------------------

The server you logged onto is a webserver.

-------------------------------------------------------------------------

This means it can serve content on the web.

-------------------------------------------------------------------------

Due to aforementioned //issues// we can only serve static files (no database)

-------------------------------------------------------------------------

This is more than enough for most folks needs.

-------------------------------------------------------------------------

Create a ``public_html`` in your own home directory with correct permissions

-------------------------------------------------------------------------

```bash
mkdir ~/public_html 
chmod 711 ~
chmod -R 755 ~/public_html
cd ~/public_html 
```

-------------------------------------------------------------------------

Now we have a folder, time to put something in it.

-------------------------------------------------------------------------

```bash
nano index.html
```

-------------------------------------------------------------------------

This brings up a terminal based editor, type whatever ye want here.

-------------------------------------------------------------------------

To exit press ``[ctrl]``+``[x]`` to bring up save options.  
You cna rename the file, for now just press ``[enter]``

-------------------------------------------------------------------------

Go to <<https://{username}.users.skynet.ie>> to view your website.

-------------------------------------------------------------------------

That was cumbersome to do right?

-------------------------------------------------------------------------

Logging in.  
Only to change a file on terminal that you have never used before.

-------------------------------------------------------------------------

Surely there is a better way?

-------------------------------------------------------------------------

Well luckily for you programmers are lazy

-------------------------------------------------------------------------

There are ways to transfer files from your local computer to the server.

-------------------------------------------------------------------------

Fow Windows you have WinSCP: <<https://winscp.net/>>

-------------------------------------------------------------------------

If ye are using linux you can use put ``sftp://{username}@skynet.skynet.ie``   
(or use filezilla)

-------------------------------------------------------------------------

## Gitlab

-------------------------------------------------------------------------

As part of your Skynet account you have access to our Gitlab Server.

-------------------------------------------------------------------------

1. Go to <<https://gitlab.skynet.ie>>
2. Login with ``username`` and ``password``
3. Done

-------------------------------------------------------------------------

## Gitlab Website

-------------------------------------------------------------------------

its possible to have a website on gitlab.

-------------------------------------------------------------------------

Powered by source control and CI/CD pipelines (devops)

-------------------------------------------------------------------------

Go to the ``Dashboard`` > ``Groups`` > ``New Group``

-------------------------------------------------------------------------

Name the group Personal or College or something like that.

-------------------------------------------------------------------------

Inside that Group create a ``New Project``, name it whatever ye want.

-------------------------------------------------------------------------

``Create from Template`` > Any one that starts with ``Pages``  
(I recommend ``pages/html``)

-------------------------------------------------------------------------

It will appear on the screen in a moment after clicking.

-------------------------------------------------------------------------

Go up to Edit > Web IDE > Change something on the index.html

-------------------------------------------------------------------------

``[ctrl] + [shift] + [g]`` (or click the icon on the left), enter a msg  
And ``commit to master``.

-------------------------------------------------------------------------

Your website will now be build and deployed.

-------------------------------------------------------------------------

Woot!!! Devops!!!

-------------------------------------------------------------------------

To get teh link is a tad annoying.

-------------------------------------------------------------------------

Go to ``Settings`` (bottom left of repo page) > Pages

-------------------------------------------------------------------------

Open the link in a new tab and marvel at yer creation.

-------------------------------------------------------------------------

If you go back and commit new changes they will show up as well.

-------------------------------------------------------------------------

In a future session we will discuss Gitlab (and git in general) more.

-------------------------------------------------------------------------

Here completes the rough introduction to Skynet and creating websites.

-------------------------------------------------------------------------

If ye have any questions/ideas feel free to ask.

-------------------------------------------------------------------------

If ye want to take a deeper look into server admin feel free to ask.

-------------------------------------------------------------------------

Skynet team is always open to newcomers!
