+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Skynet: Email"
date    = 2024-02-10
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Email

-------------------------------------------------------------------------

## Overview

-------------------------------------------------------------------------

Email is an essential part of life now

-------------------------------------------------------------------------

An email is essentially your identity online

-------------------------------------------------------------------------

Well //an// identity, you can have many

-------------------------------------------------------------------------

Thus its incredibly important

-------------------------------------------------------------------------

[This talk is incredibly useful for a good overview][1]

-------------------------------------------------------------------------

[We have SPF, DKIM and DMARK][4]

-------------------------------------------------------------------------

## Aliases

-------------------------------------------------------------------------

Delivering to //your// mailbox is all good and grand

-------------------------------------------------------------------------

But what if you want to be someone else?

-------------------------------------------------------------------------

Such as a service account, for example: ``root@skynet.ie``

-------------------------------------------------------------------------

Thankfully that is possible (if you are given access)

-------------------------------------------------------------------------

[We have aliases set up for stuff like that][2]

-------------------------------------------------------------------------

So anyone in these groups gets mail from these addresses sent to them

-------------------------------------------------------------------------

These (in our case) get sent to a subfolder in our inboxes

-------------------------------------------------------------------------

But what if we want to send mail as the service account?

-------------------------------------------------------------------------

In [Thunderbird][3] it is relatively easy

-------------------------------------------------------------------------

Sign into your Skynet account on Thunderbird

-------------------------------------------------------------------------

Select ``Settings`` (bottom right)

-------------------------------------------------------------------------

![img.png](9_email/settings.png)

-------------------------------------------------------------------------

Select ``Account Setttings``

-------------------------------------------------------------------------

![img_1.png](9_email/settings_account.png)

-------------------------------------------------------------------------

Select your Skynet email then ``Manage Identities``

-------------------------------------------------------------------------

![img_2.png](9_email/settings_account_page.png)

-------------------------------------------------------------------------

This screen has all your current Identities

-------------------------------------------------------------------------

Select ``Add``

-------------------------------------------------------------------------

![img_3.png](9_email/manage_identities.png)

-------------------------------------------------------------------------

Add yer ``Name``, ``Email Address`` you want to alias and select ``OK``

-------------------------------------------------------------------------

![img_4.png](9_email/manage_identity.png)

-------------------------------------------------------------------------

In this example I would be able to send mail as ``this_is_a_real_email@skynet.ie``

-------------------------------------------------------------------------

(if I was actually allowed to do so)

-------------------------------------------------------------------------

[//Use this time to explore the nixos repo to explain//][0]


[0]: https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/blob/26e715b2f62e406deee5e773ebcc3e3c3d200186/applications/email.nix?ref_type=heads
[1]: https://www.youtube.com/watch?v=mrGfahzt-4Q
[2]: https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/blob/26e715b2f62e406deee5e773ebcc3e3c3d200186/applications/email.nix?ref_type=heads#L31-L91
[3]: https://www.thunderbird.net/en-GB/
[4]: https://gitlab.skynet.ie/compsoc1/skynet/nixos/-/blob/26e715b2f62e406deee5e773ebcc3e3c3d200186/applications/email.nix?ref_type=heads#L314-L343