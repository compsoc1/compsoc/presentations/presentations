+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Skynet: Intro"
date    = 2023-09-23
slides  = true
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Skynet: Introduction

-------------------------------------------------------------------------

Skynet is the UL Computer Society's computer cluster.

-------------------------------------------------------------------------

Has a long history: <<https://2016.skynet.ie/history.html>>.   

-------------------------------------------------------------------------

Older than the Society. (1992 vs 1994)

-------------------------------------------------------------------------

But alas good things always come to an end.


-------------------------------------------------------------------------

Due to //A Series of Unfortunate Events// we lost internet in Jan 2023

-------------------------------------------------------------------------

Turns out Wordpress got Wordpress'd and we had a //smol// spam problem.

-------------------------------------------------------------------------

ITD also wanted us to have all servers patched and up to date.

-------------------------------------------------------------------------

# Skynet 3.0

-------------------------------------------------------------------------

Gave us a chance to do a full rebuild.  

-------------------------------------------------------------------------

Servers were delved Indiana Jones style.  

-------------------------------------------------------------------------

![Delving into Ruins](0_intro_img1.png)

-------------------------------------------------------------------------

Backups were made.

-------------------------------------------------------------------------

Returned Summer 2023.  

-------------------------------------------------------------------------

Powered by NixOS.  

-------------------------------------------------------------------------

Fully open source: <<https://gitlab.skynet.ie/compsoc1/skynet/nixos>> 

-------------------------------------------------------------------------

Skynet now can compile and update itself.  

-------------------------------------------------------------------------

Be not Afraid

-------------------------------------------------------------------------

Automation FTW!

-------------------------------------------------------------------------

How hosts:

-------------------------------------------------------------------------

* Email
* Gitlab + CI/CD runners
* Game Servers
  * Minecraft
* ULFM
* Websites
  * <<https://skynet.ie>>
  * User sites (<<https://silver.users.skynet.ie>>)

-------------------------------------------------------------------------

Always iterating, always improving.