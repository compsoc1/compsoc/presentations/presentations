+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
title   = "Skynet: Topics"
date    = 2023-10-08
slides  = false
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Topics that will be covered by teh skynet Training

* [*nix, Nix and Nixos](./3_nix.html)
* [DNS](./4_dns.html)
* [LDAP](./5_ldap.html)
* [Email](./9_email.html)
* [CI/CD](./6_cicd.html)
* [Proxmox/VM/LXC](./8_proxmox.html)
* [Databases (Sqlite)](./7_databases.html)

This list may be added to in teh future, if ye have any ideas feel free to ping.