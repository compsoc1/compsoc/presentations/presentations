# Presentations for Compsoc
Presentations for Compsoc can be found in this repo.

## Compiling
An example file is in [the src folder][1].  
Please follow this format.

To compile them run ``nix run``.  
Output will be in teh ``build`` folder.  
These will be webpages that you can open in browser.  
Up/down arrows progress it forwards/back.

Instructions to install nix can be found over in the [nixos repo][2]  
You need both nix and flakes enabled.


[1]: src/slides/example/Example.md
[2]: https://gitlab.skynet.ie/compsoc1/skynet/nixos#prep