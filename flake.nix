{
  description = "Skynet Presentations";

  inputs = {
    utils.url = "github:numtide/flake-utils";

    # nix flake lock --update-input bfom
    bfom.url = "gitlab:silver_rust/bfom";
  };

  /*
  sudo nano /etc/nix/nix.conf
  trusted-users = {username}
  */
  nixConfig = {
    extra-substituters = "https://nix-cache.skynet.ie/skynet-cache";
    extra-trusted-public-keys = "skynet-cache:OdfA4Or0JcHiHf05fsiIR4nZT2z2yDEtkoLqhntGAz4=";
  };

  outputs = {
    self,
    nixpkgs,
    bfom,
    ...
  } @ inputs: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux.pkgs;
  in {
    # nix run
    apps.x86_64-linux.default = {
      type = "app";
      program = "${bfom.defaultPackage.x86_64-linux}/bin/cargo-bfom";
    };

    # `nix build`
    packages.x86_64-linux.default = pkgs.stdenv.mkDerivation {
      name = "slides.skynet.ie";
      src = self;
      buildPhase = "${bfom.defaultPackage.x86_64-linux}/bin/cargo-bfom";
      installPhase = "mkdir -p $out; cp -R build/* $out";
    };
  };
}
